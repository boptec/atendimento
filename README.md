# Atendimento de clientes BOP Tec

Olá,

Se você chegou até aqui para fazer um pedido de atendimento, clique [aqui](https://gitlab.com/boptec/atendimento/-/issues/new) e registre lá o seu pedido.
